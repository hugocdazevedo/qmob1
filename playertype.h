#ifndef PLAYERTYPE_H
#define PLAYERTYPE_H

#include <QMetaType>

struct P2D {
    Q_GADGET
    Q_PROPERTY(float x MEMBER m_x)
    Q_PROPERTY(float y MEMBER m_y)

public:
    float m_x;
    float m_y;
};

struct Player {
    Q_GADGET

    Q_PROPERTY(PlayerType playerType MEMBER m_playerType)
    Q_PROPERTY(float speed MEMBER m_speed)
    Q_PROPERTY(quint16 ammunition MEMBER m_ammunition)
    Q_PROPERTY(bool active MEMBER m_active)
    Q_PROPERTY(quint8 numberOfCoordinates MEMBER m_numberOfCoordinates)
    Q_PROPERTY(P2D* coordinates MEMBER m_coordinates);

public:
    enum class PlayerType {
        NO_TYPE = 0,
        TANK,
        CHARACTER,
        SHIP
    };
    Q_ENUM(PlayerType)

    PlayerType m_playerType;
    float m_speed;
    quint16 m_ammunition;
    bool m_active;
    quint8 m_numberOfCoordinates;
    P2D* m_coordinates;

};

Q_DECLARE_METATYPE(Player)
Q_DECLARE_METATYPE(P2D*)

#endif // PLAYERTYPE_H
