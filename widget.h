#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QScrollArea>
#include <QFormLayout>

#include <QInputDialog>
#include <QMessageBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QFrame>

#include <QTimer>
#include <QMetaType>
#include <QMetaProperty>
#include <QMetaObject>
#include <QMetaEnum>
#include "playertype.h"
#include <QVariant>
#include <QString>

#include <QResizeEvent>
#include <QGuiApplication>
#include <QScreen>

#include <QDebug>

#define STARTUP_TIMER 50
#define SIZE_TO_TEST_POINTER 1000
#define HEX_TYPE 16
#define DEC_TYPE 10

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    QScrollArea* mainScrollArea;
    QFrame* mainFrame;
    QInputDialog* typeInput;
    QList<QFrame*>* listFrame;
    QList<QFormLayout*>* listFormLayout;
    QList<QComboBox*>* listComboBox;
    QList<QDoubleSpinBox*>* listDoubleSpinBox;
    QList<QSpinBox*>* listSpinBox;
    QList<QCheckBox*>* listCheckBox;
    QList<const QMetaObject*>* listMetaObject;

    int idOfType;
    qintptr pointerAddress;

#if QT_VERSION >= 0x060000
    QMetaType copyType;
#endif
    QMetaType* initialMetaType;
    QMetaType* initialMetaType2;
    QMetaType* metatype;

    static void registerTypes();
    void startForm();
    void sendPointerToWidget(QMetaType* t_mt, int t_index);
    static QString addIndexToName(QString t_name, int t_index);
    void sendGadgetToWidget(QMetaType* t_mt, int t_index);
    void sendMetaPropertyToWidget(QMetaType* t_mt, int t_index, int loop_index = 0);
    static bool isPointerToStruct(const QString& t_name);

public slots:
    void getTypeAndPointer();
    void sendValueChanged();

private:
    void resizeEvent(QResizeEvent *ev);
};
#endif // WIDGET_H
