# Considerações sobre a atividade 1

## Implementações pendentes
- [ ] Alocação de memória dos ponteiros de dimensão variável (comentários no código)
- [ ] Tratamento para lista de enum
- [ ] Tratamento para lista de struct
- [ ] Revisão na função QMetaType.construct() compilada no Qt6 (problema com acesso à memória)

## Versões do Qt, CMake e sistema operacional

### Qt
- Testado corretamente na versão 5.15.2.
- Testado com falha de memória (QMetaType.construct()) na versão 6.1.2

### CMake
- Versão 3.16.3

### Sistema operacional
- Kubuntu 20.04
