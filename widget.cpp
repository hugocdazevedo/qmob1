#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent), mainScrollArea(new QScrollArea(this)), listFrame(new QList<QFrame*>),
      listFormLayout(new QList<QFormLayout*>), listComboBox(new QList<QComboBox*>),
      listDoubleSpinBox(new QList<QDoubleSpinBox*>), listSpinBox(new QList<QSpinBox*>),
      listCheckBox(new QList<QCheckBox*>), listMetaObject(new QList<const QMetaObject*>),
      idOfType(0), pointerAddress(0)
{
    QTimer::singleShot(STARTUP_TIMER, this, SLOT(getTypeAndPointer()));
    registerTypes();
}

Widget::~Widget()
{
    delete listFrame;
    delete listFormLayout;
    delete listComboBox;
    delete listDoubleSpinBox;
    delete listSpinBox;
    delete listCheckBox;
    delete listMetaObject;
    delete initialMetaType;
    delete metatype;
}

void Widget::registerTypes()
{
    qRegisterMetaType<Player>("Player");
    qRegisterMetaType<P2D*>("P2D*");
}

void Widget::getTypeAndPointer()
{
    //GetTypeName
    bool ok = false;
    QString text = "";
    do {
        text = QInputDialog::getText(this, tr("Initial Input"),
                                     tr("Type name:"), QLineEdit::Normal,
                                     "Player", &ok);
        //Verify previous register of type
        if (ok) {
#if QT_VERSION >= 0x060000
            copyType = QMetaType::fromName(text.toLatin1());
            if (!copyType.isValid())
                ok = false;
#else
            idOfType = QMetaType::type(text.toLatin1());
            if (idOfType == QMetaType::UnknownType) {
                ok = false;
            }
#endif
        }

    } while (!ok || text.isEmpty());

    //MetaType for execution time use
#if QT_VERSION >= 0x060000
    initialMetaType = new QMetaType(copyType.id());
#else
    initialMetaType = new QMetaType(idOfType);
#endif
    //GetPointerAddress
    ok = false;
    QString text2 = "";
    do {
        text2 = QInputDialog::getText(this, tr("Initial Input"),
                                             tr("Pointer address (hexadecimal):"), QLineEdit::Normal,
                                             QString::number((qintptr)initialMetaType, HEX_TYPE), &ok);
        if (ok) {
            pointerAddress = (qintptr)text2.toULongLong(&ok, HEX_TYPE);//After - test pointer address
        }

    } while (!ok || text.isEmpty());

    //Start main form
    startForm();

    //Define geometry based on initialized layout
    mainScrollArea->setWidget(listFrame->first());

    //when refresh form
    this->resize(listFrame->first()->sizeHint().width()+listFrame->first()->contentsMargins().left()+listFrame->first()->contentsMargins().right(), listFrame->first()->sizeHint().height()+listFrame->first()->contentsMargins().top()+listFrame->first()->contentsMargins().bottom());
    this->setMaximumSize(((QScreen*)(QGuiApplication::primaryScreen()))->geometry().width()-QWidget::frameGeometry().x(), ((QScreen*)(QGuiApplication::primaryScreen()))->geometry().height()-QWidget::frameGeometry().y());
}

void Widget::startForm()
{
    //Main Frame
    QFrame *frame = new QFrame(this);
    frame->setFrameShape(QFrame::Box);
    listFrame->append(frame);

    //Main FormLayout
    QFormLayout *formLayout = new QFormLayout(frame);
    listFormLayout->append(formLayout);

    //MetaType for execution time use
#if QT_VERSION >= 0x060000
    //We should be investigate problem with memory allocation using Qt6
    initialMetaType2 = (QMetaType*)initialMetaType->construct((void*)pointerAddress, (void const*)initialMetaType);
#else
    initialMetaType = (QMetaType*)initialMetaType->construct((void*)pointerAddress, nullptr);
#endif
    //Get MetaObject and MetaProperties from MetaType
    const QMetaObject *ob = initialMetaType->metaObject();

    //Scan properties
    int count = ob->propertyCount();
    for (int i = 0; i < count; ++i) {
        QMetaProperty metaproperty = ob->property(i);

        //isPointer?
        if (QString::fromLocal8Bit(metaproperty.typeName()).contains("*")) {
            sendPointerToWidget(initialMetaType, i);
        }
        //isUserType?
        if (metaproperty.userType() >= QMetaType::User) {
//            sendGadgetToWidget(metatype, i);
        }
        //isCommonProperty?
        else {
            sendMetaPropertyToWidget(initialMetaType, i);
        }
    }
}

void Widget::sendPointerToWidget(QMetaType *t_mt, int t_index)
{
    //Get parent MetaObject and MetaProperty
    const QMetaObject *ob = t_mt->metaObject();
    listMetaObject->append(ob);

    QMetaProperty metaproperty = ob->property(t_index);

    //Create a frame to pointer argument
    QFrame *frame = new QFrame(this);
    frame->setFrameShape(QFrame::Box);
    listFrame->append(frame);
    (listFormLayout->last())->addRow(metaproperty.name(), frame);

    //Add FormLayout to Frame
    QFormLayout *formLayout = new QFormLayout(frame);
    listFormLayout->append(formLayout);

    //Get child MetaObject
#if QT_VERSION >= 0x060000
    QMetaType copyMetaType = QMetaType::fromName(QString(metaproperty.typeName()).toLatin1());
    metatype = new QMetaType(copyMetaType.id());
#else
    metatype = new QMetaType(QMetaType::type(QString(metaproperty.typeName()).toLatin1()));
#endif
    const QMetaObject *ob1 = metatype->metaObject();
    listMetaObject->append(ob1);

    //Get argument quantity by last parameter
    int count = ob->property(t_index-1).readOnGadget(t_mt).toUInt();

    //To connect the SpinBox to this pointer
    listSpinBox->last()->setProperty("hasReference", bool(true));
    listSpinBox->last()->setProperty("formIndex", uint(listFormLayout->size()-1));
    listSpinBox->last()->setProperty("pointerReference", qulonglong(t_mt));
    listSpinBox->last()->setProperty("indexReference", uint(t_index));

    //Get current FormLayout to insert new items
    int formIndex = listFormLayout->size()-1;

    //Loop with size of list
    for (int i = 0; i < count; ++i) {
        //If the argument is  a pointer of a struct
        if (isPointerToStruct(QString(metaproperty.typeName()).toLatin1())) {

            //New Frame and FormLayout for each instance of this property
            QFrame *frame = new QFrame(this);
            frame->setFrameShape(QFrame::Box);

            //Get parent layout
            listFormLayout->at(formIndex)->addRow(addIndexToName(metaproperty.name(), i+1), frame);
            QFormLayout *formLayout = new QFormLayout(frame);
            listFormLayout->append(formLayout);

            //Scan properties of each gadget
            int count2 = ob1->propertyCount();
            for (int j = 0; j < count2; ++j) {
                QMetaProperty metaproperty = ob1->property(j);

                //isPointer?
                if (QString::fromLocal8Bit(metaproperty.typeName()).contains("*")) {
                    sendPointerToWidget(metatype, j);
                }
                //isUserType?
                if (metaproperty.userType() >= QMetaType::User) {
//                    sendGadgetToWidget(metatype, i);
                }
                //isCommonProperty?
                else {
                    sendMetaPropertyToWidget(metatype, j);
                }
            }
        }
        else {
            //If argument is a simple bool, uint, or float
            sendMetaPropertyToWidget(t_mt, t_index, i+1);
        }
    }
}

QString Widget::addIndexToName(QString t_name, int t_index)
{
    return ((bool)t_index ? t_name.remove(t_name.size()-1,1).append(QString::number(t_index, DEC_TYPE)) : t_name);
}

//void Widget::sendGadgetToWidget(QMetaType *t_mt, int t_index)
//{
//}

void Widget::sendMetaPropertyToWidget(QMetaType *t_mt, int t_index, int loop_index)
{
    const QMetaObject *t_object = t_mt->metaObject();

    //isKnownType?
    int userType = t_object->property(t_index).userType();
    QString typeName = t_object->property(t_index).typeName();

    //if argument is a float or float*
    if (typeName.contains("float")) {
        QDoubleSpinBox* item = new QDoubleSpinBox(this);
        item->setValue(t_object->property(t_index).readOnGadget(t_mt).toDouble());
        connect(item, SIGNAL(valueChanged(double)), this, SLOT(sendValueChanged()));

        item->setProperty("pointer", qulonglong(t_mt));
        item->setProperty("index", uint(t_index));
        (listFormLayout->last())->addRow(addIndexToName(t_object->property(t_index).name(), loop_index), item);
        return;
    }

    //if argument is a quint8 (UChar), quint16 (UShort), quint8*,or quint16*
    if (userType == QMetaType::UShort || userType == QMetaType::UChar || typeName.contains("quint8") || typeName.contains("quint16")) {
        QSpinBox* item = new QSpinBox(this);
        item->setValue(t_object->property(t_index).readOnGadget(t_mt).toUInt());
        connect(item, SIGNAL(valueChanged(int)), this, SLOT(sendValueChanged()));
        listSpinBox->append(item);

        item->setProperty("hasReference", bool(false));
        item->setProperty("pointer", qulonglong(t_mt));
        item->setProperty("index", uint(t_index));
        (listFormLayout->last())->addRow(addIndexToName(t_object->property(t_index).name(), loop_index), item);
        return;
    }

    //if argument is a bool, or bool*
    if (typeName.contains("bool")) {
        QCheckBox* item = new QCheckBox(this);
        item->setChecked(t_object->property(t_index).readOnGadget(t_mt).toBool());
        connect(item, SIGNAL(stateChanged(int)), this, SLOT(sendValueChanged()));

        item->setProperty("pointer", qulonglong(t_mt));
        item->setProperty("index", uint(t_index));
        (listFormLayout->last())->addRow(addIndexToName(t_object->property(t_index).name(), loop_index), item);
        return;
    }

    //if argument is a enum, or enum* (enum* DOES NOT WORK)
    if (t_object->property(t_index).isEnumType() || typeName.contains("*")) {
        QComboBox* item = new QComboBox(this);
        int count = t_object->enumeratorCount();
        for (int x = 0; x < count; x++) {
            if (t_object->enumerator(x).enumName() == t_object->property(t_index).typeName()) {
                for (int y = 0; y < t_object->enumerator(x).keyCount(); y++) {
                    item->addItem(t_object->enumerator(x).key(y));
                }
                break;
            }
        }

        listComboBox->append(item);
        item->setCurrentIndex(t_object->property(t_index).readOnGadget(t_mt).toUInt());
        connect(item, SIGNAL(currentIndexChanged(int)), this, SLOT(sendValueChanged()));

        item->setProperty("pointer", qulonglong(t_mt));
        item->setProperty("index", uint(t_index));
        (listFormLayout->last())->addRow(addIndexToName(t_object->property(t_index).name(), loop_index), item);
    }
}

bool Widget::isPointerToStruct(const QString& t_name)
{
    return !(t_name.contains("quint")||t_name.contains("float")||t_name.contains("bool"));
}

void Widget::sendValueChanged()
{
    if (sender()->inherits("QComboBox")) {
         QComboBox* tmp1 = (QComboBox*)sender();

         QMetaType *mt = (QMetaType*)tmp1->property("pointer").toULongLong();
         const QMetaObject *t_object = mt->metaObject();
         QMetaProperty property = t_object->property(tmp1->property("index").toUInt());

         property.writeOnGadget(mt, tmp1->currentIndex());
    }

    if (sender()->inherits("QDoubleSpinBox")) {
         QDoubleSpinBox* tmp1 = (QDoubleSpinBox*)sender();

         QMetaType *mt = (QMetaType*)tmp1->property("pointer").toULongLong();
         const QMetaObject *t_object = mt->metaObject();
         QMetaProperty property = t_object->property(tmp1->property("index").toUInt());

         property.writeOnGadget(mt, tmp1->value());
    }
    if (sender()->inherits("QSpinBox")) {
         QSpinBox* tmp1 = (QSpinBox*)sender();

         QMetaType *mt = (QMetaType*)tmp1->property("pointer").toULongLong();
         const QMetaObject *t_object = mt->metaObject();
         QMetaProperty property = t_object->property(tmp1->property("index").toUInt());

         property.writeOnGadget(mt, tmp1->value());

         //if changed value changes a list
         if (tmp1->property("hasReference").toBool()) {
             QMetaType *t_mt = (QMetaType*)tmp1->property("pointerReference").toULongLong();
             const QMetaObject * ob = t_mt->metaObject();
             QMetaProperty metaPropertyReference = ob->property(tmp1->property("indexReference").toUInt());
             uint formIndex  = tmp1->property("formIndex").toUInt();

             int rowsCount_new = tmp1->value();
             int rowsCount_old = ((QFormLayout*)listFormLayout->at(formIndex))->rowCount();

             //Remove itens
             //MY FUTURE APPROACH HERE:
             //we should be free t_mt.sizeOf() at the end of allocation from t_mt pointer.Here we should be Free the t_mt.sizeOf() at the end of allocation from t_mt pointer.
             if (rowsCount_new < rowsCount_old) {
                 while (rowsCount_new != rowsCount_old) {
                     ((QFormLayout*)listFormLayout->at(formIndex))->removeRow(--rowsCount_old);
                     mainScrollArea->setFixedSize(this->size());
                     listFrame->first()->setFixedSize(listFrame->first()->sizeHint().width() > this->size().width() ? listFrame->first()->sizeHint().width() : this->size().width()-listFrame->first()->contentsMargins().left()-listFrame->first()->contentsMargins().right(),
                                                      listFrame->first()->sizeHint().height() > this->size().height() ? listFrame->first()->sizeHint().height() : this->size().height()-listFrame->first()->contentsMargins().top()-listFrame->first()->contentsMargins().bottom());
                                      }
             }
             //Add itens
             //MY FUTURE APPROACH HERE:
             //we should be allocate rowsCount_new*t_mt.sizeOf();
             //copy rowsCount_old*t_mt.sizeOf() of t_mt pointer to new pointer;
             //free memory of rowsCount_old*t_mt.sizeOf() of t_mt pointer;
             //point t_mt to new allocated memory space.
             else {
                 //Get MetaType from MetaProperty
#if QT_VERSION >= 0x060000
    QMetaType copyMetaType = QMetaType::fromName(QString(metaPropertyReference.typeName()).toLatin1());
    QMetaType *t_metatype = new QMetaType(copyMetaType.id());
#else
                 QMetaType *t_metatype = new QMetaType(QMetaType::type(QString(metaPropertyReference.typeName()).toLatin1()));
#endif
                 const QMetaObject *ob1 = t_metatype->metaObject();

                 for (int i = rowsCount_old; i < rowsCount_new; i++) {
                     QFrame *frame = new QFrame(this);
                     frame->setFrameShape(QFrame::Box);

                     listFormLayout->at(formIndex)->addRow(addIndexToName(metaPropertyReference.name(), i+1), frame);
                     QFormLayout *formLayout = new QFormLayout(frame);
                     listFormLayout->append(formLayout);

                     int count2 = ob1->propertyCount();
                     for (int j = 0; j < count2; ++j) {
                         QMetaProperty metaproperty = ob1->property(j);

                         //isPointer?
                         if (QString::fromLocal8Bit(metaproperty.typeName()).contains("*")) {
                             sendPointerToWidget(t_metatype, j);
                         }
                         //isUserType?
                         if (metaproperty.userType() >= QMetaType::User) {

                         }
                         //isCommonProperty?
                         else {
                             sendMetaPropertyToWidget(t_metatype, j);
                         }
                     }

                     listFrame->first()->setFixedSize(listFrame->first()->sizeHint().width() > this->size().width() ? listFrame->first()->sizeHint().width() : this->size().width()-listFrame->first()->contentsMargins().left()-listFrame->first()->contentsMargins().right(),
                                                      listFrame->first()->sizeHint().height() > this->size().height() ? listFrame->first()->sizeHint().height() : this->size().height()-listFrame->first()->contentsMargins().top()-listFrame->first()->contentsMargins().bottom());
                     this->resize(listFrame->first()->sizeHint().width() > this->size().width() ? listFrame->first()->sizeHint().width()+listFrame->first()->contentsMargins().left()+listFrame->first()->contentsMargins().right() : this->size().width(),
                                  listFrame->first()->sizeHint().height() > this->size().height() ? listFrame->first()->sizeHint().height()+listFrame->first()->contentsMargins().top()+listFrame->first()->contentsMargins().bottom() : this->size().height());

                 }
                 delete t_metatype;
             }
         }
    }

    if (sender()->inherits("QCheckBox")) {
         QCheckBox* tmp1 = (QCheckBox*)sender();

         QMetaType *mt = (QMetaType*)tmp1->property("pointer").toULongLong();
         const QMetaObject *t_object = mt->metaObject();
         listMetaObject->append(t_object);
         QMetaProperty property = t_object->property(tmp1->property("index").toUInt());

         property.writeOnGadget(mt, tmp1->checkState());
    }
}

void Widget::resizeEvent(QResizeEvent *ev)
{
    mainScrollArea->setFixedSize(this->size());
    if (!listFrame->isEmpty()) {
        listFrame->first()->setFixedSize(listFrame->first()->sizeHint().width() > ev->size().width() ? listFrame->first()->sizeHint().width() : ev->size().width()-listFrame->first()->contentsMargins().left()-listFrame->first()->contentsMargins().right(),
                                         listFrame->first()->sizeHint().height() > ev->size().height() ? listFrame->first()->sizeHint().height() : ev->size().height()-listFrame->first()->contentsMargins().top()-listFrame->first()->contentsMargins().bottom());
    }
}
